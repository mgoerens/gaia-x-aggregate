package main

import (
	"context"
	"gaia-x-aggregate/internal/app/aggregate"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	mainCtx, cancel := context.WithCancel(context.Background())

	go func() {
		termChan := make(chan os.Signal)
		signal.Notify(termChan, syscall.SIGINT, syscall.SIGTERM)
		<-termChan
		cancel()
	}()

	aggregate.Run(mainCtx)
}
