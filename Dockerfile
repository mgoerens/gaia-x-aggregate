FROM golang:alpine
RUN mkdir /app
ADD . /app/
WORKDIR /app
RUN go build -o main ./cmd/
RUN adduser -S -D -H -h /app appuser
EXPOSE 8080
USER appuser
CMD ["./main"]