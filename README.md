# Installation
Start docker  
`docker run -d --name timescaledb -p 5432:5432 -e POSTGRES_PASSWORD=password timescale/timescaledb:latest-pg11`
Add database gaiax and schema data
`docker exec -it timescaledb psql -U postgres -c "create database gaiax;" && docker exec -it timescaledb psql -U postgres -d gaiax -c "create schema data;"`
The table will be created on startup

Sample http call
```
curl "localhost:8080/sensordata/temperature/pump-1/sensor-tv/?startDate=0&endDate=9999999999"|jq
```
