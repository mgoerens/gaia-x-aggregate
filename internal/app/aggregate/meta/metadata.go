package meta

const Raw = `
{
  "@context": {
    "dcat": "http://www.w3.org/ns/dcat#",
    "dct": "http://purl.org/dc/terms/",
    "dcterms": "http://purl.org/dc/terms/",
    "gaia": "https://url-to-gaia-vocab/",
    "geo": "http://www.w3.org/2003/01/geo/wgs84_pos#",
    "ids": "https://w3id.org/idsa/core/",
    "idsc": "https://w3id.org/idsa/code/",
    "owl": "http://www.w3.org/2002/07/owl#",
    "qudt-1-1": "http://qudt.org/1.1/schema/qudt#",
    "qudt-unit-1-1": "http://qudt.org/1.1/vocab/unit#",
    "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
    "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
    "sosa": "http://www.w3.org/ns/sosa#",
    "ssn": "http://www.w3.org/ns/ssn#",
    "time": "http://www.w3.org/2006/time#",
    "xsd": "http://www.w3.org/2001/XMLSchema#"
  },
  "@graph": [
    {
      "@id": "_:ub974bL89C23",
      "@type": "time:Instant",
      "time:inXSDDateTimeStamp": {
        "@type": "xsd:dateTimeStamp",
        "@value": "2020-01-01T00:00:00+00:00"
      }
    },
    {
      "@id": "_:f4f227becf5c2490b94a1d20eb8b63aa5b3",
      "@type": "sosa:FeatureOfInterest",
      "geo:alt": {
        "@type": "xsd:decimal",
        "@value": "12.75"
      },
      "geo:lat": {
        "@type": "xsd:decimal",
        "@value": "35.8648067"
      },
      "geo:long": {
        "@type": "xsd:decimal",
        "@value": "-120.6195831"
      },
      "rdfs:label": "Frankfurt main station"
    },
    {
      "@id": "_:ub974bL92C17",
      "@type": "time:Instant",
      "time:inXSDDateTimeStamp": {
        "@type": "xsd:dateTimeStamp",
        "@value": "2020-02-01T00:00:00+00:00"
      }
    },
    {
      "@id": "_:ub974bL82C18",
      "@type": "qudt-1-1:QuantityValue",
      "qudt-1-1:unit": {
        "@id": "qudt-unit-1-1:Pascal"
      }
    },
    {
      "@id": "_:ub974bL70C14",
      "@type": "gaia:PaymentInformation",
      "gaia:currency": "Euro",
      "gaia:price_amount": 10000
    },
    {
      "@id": "_:ub974bL87C23",
      "@type": "time:Interval",
      "time:hasBeginning": {
        "@id": "_:ub974bL89C23"
      },
      "time:hasEnd": {
        "@id": "_:ub974bL92C17"
      }
    },
    {
      "@id": "_:f4f227becf5c2490b94a1d20eb8b63aa5b1",
      "@type": "sosa:Observation",
      "sosa:hasFeatureOfInterest": {
        "@id": "_:f4f227becf5c2490b94a1d20eb8b63aa5b3"
      },
      "sosa:hasResult": {
        "@id": "_:ub974bL82C18"
      },
      "sosa:madeBySensor": {
        "@id": "_:f4f227becf5c2490b94a1d20eb8b63aa5b2"
      },
      "sosa:phenomenonTime": {
        "@id": "_:ub974bL87C23"
      },
      "sosa:resultTime": {
        "@type": "xsd:dateTimeStamp",
        "@value": "2020-02-01T00:00:12+00:00"
      },
      "sosa:sosa:observedProperty": {
        "@id": "_:f4f227becf5c2490b94a1d20eb8b63aa5b4"
      }
    },
    {
      "@id": "_:f4f227becf5c2490b94a1d20eb8b63aa5b2",
      "@type": "sosa:Sensor",
      "rdfs:label": {
        "@language": "en",
        "@value": "DHT22 Sensor"
      },
      "sosa:isHostedBy": {
        "@id": "http://comapny.com"
      },
      "sosa:madeObservation": {
        "@id": "_:f4f227becf5c2490b94a1d20eb8b63aa5b1"
      },
      "sosa:observes": {
        "@id": "_:f4f227becf5c2490b94a1d20eb8b63aa5b3"
      }
    },
    {
      "@id": "_:ub974bL31C33",
      "@type": "gaia:ServiceParameter",
      "gaia:http_representation": [
        "path_parameter",
        "query_parameter"
      ],
      "gaia:key": [
        "machine_id",
        "/topic",
        "sensor_id",
        "starttime",
        "endtime"
      ],
      "gaia:parameterPosition": [
        1,
        2,
        0
      ],
      "gaia:valueRange": [
        1681073033,
        1581078993,
        "vibration",
        "temperature",
        "sensor-tv",
        "pump-1"
      ],
      "rdfs:label": [
        "Parameter describing the endtime",
        "Parameter to select the topic",
        "Parameter to select the machine",
        "Parameter to select the sensor",
        "Parameter describing the starttime"
      ]
    },
    {
      "@id": "gaia:sample_Service:",
      "@type": "gaia:Service",
      "dcterms:description": "Some air pressure data measured at Frankfurt main station",
      "dcterms:publisher": "John Doe",
      "dcterms:title": "Sensor data provided via REST endpoint ...",
      "gaia:endpoint": {
        "@id": "_:ub974bL24C17"
      },
      "gaia:price": {
        "@id": "_:ub974bL70C14"
      },
      "gaia:serviceDescription": {
        "@id": "_:f4f227becf5c2490b94a1d20eb8b63aa5b1"
      }
    },
    {
      "@id": "_:f4f227becf5c2490b94a1d20eb8b63aa5b4",
      "@type": "sosa:ObservableProperty",
      "rdfs:comment": {
        "@language": "en",
        "@value": "The air pressure as an observable property."
      },
      "rdfs:label": {
        "@language": "en",
        "@value": "air Pressure"
      },
      "sosa:isObservedBy": {
        "@id": "_:f4f227becf5c2490b94a1d20eb8b63aa5b2"
      }
    },
    {
      "@id": "_:ub974bL24C17",
      "@type": "gaia:Endpoint",
      "gaia:endpointDocument": "<some URL to documentation>",
      "gaia:endpointHost": "http://gaia-x-aggregate-git-user2-aggregation.apps.ocp4.keithtenzer.com/sensordata",
      "gaia:endpointParameters": {
        "@id": "_:ub974bL31C33"
      },
      "gaia:endpointProtocol": "REST"
    }
  ]
}`
