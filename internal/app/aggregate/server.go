package aggregate

import (
	"context"
	"database/sql"
	"fmt"
	"gaia-x-aggregate/internal/app/aggregate/meta"
	MQTT "github.com/eclipse/paho.mqtt.golang"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

var db *sql.DB

//define a function for the default message handler
var handleMessage MQTT.MessageHandler = func(client MQTT.Client, msg MQTT.Message) {
	//fmt.Printf("TOPIC: %s\n", msg.Topic())
	//fmt.Printf("MSG: %s\n", msg.Payload())
	parts := strings.Split(string(msg.Payload()), ",")
	machineid := parts[0]
	sensorid := parts[1]
	// parse value
	value, err := strconv.ParseFloat(parts[2], 64)
	if err != nil {
		log.Fatalf("could not parse payload value: %v", err)
	}
	// parse timestamp

	timestamp := stringToTime(parts[3])

	_, err = db.Exec(`INSERT INTO data.sensors(topic, value, timestamp, machine_id, sensor_id)
	VALUES($1, $2, $3, $4, $5)`, msg.Topic(), value, timestamp, machineid, sensorid)
	if err != nil {
		log.Fatalf("can't insert: %v", err)
	}

	//log.Printf("mid: %s, sid: %s, v: %f, t: %v", machineid, sensorid, value, timestamp)
}

func stringToTime(s string) time.Time {
	// parse timestamp
	timeInt, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		log.Fatalf("could not parse payload timestamp to int: %v", err)
	}
	return time.Unix(timeInt, 0).UTC()
}

func Run(ctx context.Context) {
	go startHttpServer(ctx)
	go connectToDB(ctx)
	consume(ctx)
}

func startHttpServer(ctx context.Context) {

	router := getRouter()
	srv := &http.Server{
		Addr:    ":8080",
		Handler: router,
	}

	go func() {
		// service connections
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	<-ctx.Done()

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown: ", err)
	}

	log.Println("Server exiting")
}

type valueStruct struct {
	Value     float64 `json:"value"`
	Timestamp int64   `json:"timestamp"`
}

func getRouter() *gin.Engine {
	router := gin.Default()
	router.GET("/meta", func(c *gin.Context) {
		c.Data(http.StatusOK, "application/json", []byte(meta.Raw))
	})

	router.GET("/sensordata/:topic/:machineId/:sensorId/", func(c *gin.Context) {
		receivedTime := time.Now()
		topic := c.Param("topic")
		machineId := c.Param("machineId")
		sensorId := c.Param("sensorId")

		startDate := c.Query("startDate")
		endDate := c.Query("endDate")

		startTimestamp := stringToTime(startDate)
		endTimestamp := stringToTime(endDate)

		rows, err := db.Query(`SELECT value, timestamp from gaiax.data.sensors s WHERE 
			s.sensor_id = $1 
			AND s.machine_id = $2 
			AND s.topic LIKE '%' || $3 
			AND s.timestamp 
			BETWEEN $4 AND $5
			ORDER BY s.timestamp;`,
			sensorId, machineId, topic, startTimestamp, endTimestamp)

		if err != nil {
			log.Fatal(err)
		}

		var values []valueStruct

		for rows.Next() {
			s := struct {
				Value     float64   `json:"value"`
				Timestamp time.Time `json:"timestamp"`
			}{}
			if err := rows.Scan(&s.Value, &s.Timestamp); err != nil {
				log.Fatal(err)
			}
			values = append(values, valueStruct{
				Value:     s.Value,
				Timestamp: s.Timestamp.Unix(),
			})
		}
		c.JSON(http.StatusOK, gin.H{
			"raw": gin.H{
				"identification": gin.H{
					"machine_id":       machineId,
					"sensor_id":        sensorId,
					"topic":            topic,
					"start_track_time": startTimestamp.Unix(),
					"end_track_time":   endTimestamp.Unix(),
					"received_time":    receivedTime.Unix(),
				},
				"list_values": values,
			},
		})
	})
	return router
}

func consume(ctx context.Context) {
	//create a ClientOptions struct setting the broker address, clientid, turn
	//off trace output and set the default message handler
	opts := MQTT.NewClientOptions().AddBroker("ws://broker-amq-mqtt-all-0-svc-rte-iotdemo.apps.ocp4.keithtenzer.com:80")
	opts.SetClientID("go-simple")
	opts.SetDefaultPublishHandler(handleMessage)

	//create and start a client using the above ClientOptions
	c := MQTT.NewClient(opts)
	if token := c.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	//subscribe to the topic /go-mqtt/sample and request messages to be delivered
	//at a maximum qos of zero, wait for the receipt to confirm the subscription
	if token := c.Subscribe("#", 0, nil); token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
		os.Exit(1)
	}

	<-ctx.Done()

	//unsubscribe from /go-mqtt/sample
	if token := c.Unsubscribe("#"); token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
		os.Exit(1)
	}

	c.Disconnect(250)
}

func connectToDB(ctx context.Context) {
	connStr := os.Getenv("CONNECTION_STR")
	//"user=postgres password=password dbname=gaiax sslmode=disable"
	var err error
	db, err = sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
	if err := db.Ping(); err != nil {
		log.Fatalf("can't connect to db: %v", err)
	}

	// create the table if not exists
	_, err = db.Exec(`
		create table if not exists data.sensors
	(
		id         serial not null
	constraint sensors_pk
	primary key,
		topic      varchar,
		value      double precision,
		timestamp  timestamp,
		machine_id varchar,
		sensor_id  varchar
	);`)
	if err != nil {
		log.Fatalf("can't create database", err)
	}

	<-ctx.Done()
	db.Close()
}
