module gaia-x-aggregate

go 1.11

require (
	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/gin-gonic/gin v1.5.0
	github.com/lib/pq v1.3.0
	golang.org/x/net v0.0.0-20200202094626-16171245cfb2 // indirect
)
